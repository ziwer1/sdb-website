<?php 
include("./subViews/header1.php");
?>
<title>About Us</title>
<?php 
include("./subViews/header2.php");
?>


<div class="jumbotron white_text">
<div class="container">
<h2>ABOUT US</h2>
</div>
</div>




<div class="full_width padding_bottom">
<div class="container">


<div class="row">

<div class="col-sm-2">
</div>

<div class="col-sm-8">
<div class="panel panel-default">
<div class="panel-body">

<h2 class="blue_text">Introduction</h2>
<p>
SDB Investments Limited is a one of the leading goods transportation companies in the country, providing it's customers premium freight services to anywhere East Africa. We also buy and sell agricultural produce.
</p>
<p>
SDB investments Limited was founded 15 years ago and quickly became one of Uganda's leading moving & transpotation company with clients and partners across East Africa.
</p>

<h2 class="blue_text">Principles</h2>

<p>Principles of the company are unique to all the places in which we work:</p>
<ul>
<li><strong>Customer Satisfaction:</strong> At SDB investments Limited, customer satisfaction ranks highly on our list of priorities. Our customers mean everything to us. We believe that, "To give real service you must add something which cannot be bought or measured with money, and that is sincerity and integrity. " --Don Alden Adams.</li>
<li><strong>Quick, on-time delivery:</strong> On-time delivery is critical in our business and yours. To meet your on-time requirements consistently, we identify the best ground routes. At SDB Investments Limited, we optimize transportation and reduce waste by providing reduced costs and improved labour productivity to your business.</li>
<li><strong>Cargo Safety:</strong> SDB Investments Limited is very attentive to cargo safety. As accidents are more likely to happen with oversized cargoes, we provide a detailed cargo assessment to ensure cargo safety on board.</li>
<li><strong>Quality comes first!:</strong> In the end, that is the basic assumption for all jobs, and also a principle of our company is that your goods transportation will be carried out professionally, with quality and affordable.</li>
<li><strong>We value your belongings!</strong> Our trucks are equipped with GPS technology which allows us to track them wherever they may be.</li>
</ul>     


<h2 class="blue_text">Mission</h2>

<p>
Mission of the company since it was established was to satisfy customer who we guarantee discretion and reliable and uninterrupted flow of carrying out the services we provide.
</p>

<p>
Our goal is to provide quality services that are similar to the world's leading companies in this field, which confirms our cooperation with clients across East Africa. 
</p>

<h2 class="blue_text">Vision</h2>
<p>
Our vision is that our company, in addition to good positioning in the domestic market, take a leadership position only
through the quality system of services and to become the leader in providing transportation services in Sub-Saharan Africa.
</p>

<h2 class="blue_text">Guarantee</h2>
<p>
Our greatest guarantee is represented by satisfied customers. The best quality of our work will be assured when you look at our list of customers and recommendations we receive from them. It will be our pleasure if you and your recommendations will be found among them. 
</p>
















</div>
</div>
</div>
</div>

<div class="col-sm-2">
</div>


</div><!-- container div -->
</div><!-- full_width div -->




<?php
include("./subViews/footer.php");
?>