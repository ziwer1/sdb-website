<meta name="viewport" content="width=device-width, initial-scale=1.0", maximum-scale=1.0, user-scalable=no>
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-custom.css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.9.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<div id="logo_div_wrapper">
<div class="container">

<div class="logo_divs text-left blue_text " id="logo_div_2">
<table>
<tr><td>+256 701 990 410</td></tr>
<tr><td>+256 702 558 078</td></tr>
<tr><td>sbdinvestments@gmail.com</td></tr>
<tr><td>Plot No. 590 Wankulukuku Road</td></tr>
<tr><td>Nalukolongo, Kampala, Uganda</td></tr>

</table>
</div>

<div class="logo_divs" id="logo_div_1">
<a href="index.php"><img src="images/logo3.png" id="logo" class="img-responsive"></a>
</div>


</div>
</div>

<!--
<div id="logo_div_wrapper">
<div class="container white_text" id="logo_div">
<h1>SDB</h1>
<h1>INVESTMENTS</h1>
</div>
</div>
-->

<nav class="navbar navbar-default navbar-static-top navbar-inverse" role="navigation">
<div class="container">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="index.php">Home</a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

<ul class="nav navbar-nav navbar-right">
<li><a href="about_us.php">About Us</a></li>
<li><a href="clients.php">Our Clients</a></li>
<li><a href="contact_us.php">Contact Us</a></li>
</ul>
</li>
</ul>
</div><!-- /.navbar-collapse -->
</div>
</nav>

