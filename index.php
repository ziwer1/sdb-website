<?php 
include("./subViews/header1.php");
?>
<title>Home</title>
<?php 
include("./subViews/header2.php");
?>


<div class="container">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
<!-- Indicators -->
<ol class="carousel-indicators">
<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
<li data-target="#carousel-example-generic" data-slide-to="1"></li>
<li data-target="#carousel-example-generic" data-slide-to="2"></li>
<li data-target="#carousel-example-generic" data-slide-to="3"></li>
<li data-target="#carousel-example-generic" data-slide-to="4"></li>
</ol>
<!-- Wrapper for slides -->
<div class="carousel-inner">


<div class="item active">
<img src="images/export.jpg"  alt="">
<div class="carousel-caption">
<h1>Export & Import</h1>
<p><strong>Export</strong> and <strong>Import</strong> your goods anywhere in East Africa</p>
</div>
</div>

<div class="item">
<img src="images/sdb_trucks1.jpg" alt="">
<div class="carousel-caption">
<h1>Mercedez-Benz Actros</h1>
<p>We use the highly reliable <strong>Mercedes-Benz Actros</strong> trucks to transport your goods.</p>
</div>
</div>

<div class="item">
<img src="images/produce.jpg" alt="">
<div class="carousel-caption">
<h1>Agricultural Produce</h1>	
<p>We buy and sell agricultural produce</p>
</div>
</div>


<div class="item">
<img src="images/gps.jpg" alt="">
<div class="carousel-caption">
<h1>GPS Tracking</h1>
<p>GPS satellite tracking for all our trucks.</p>
</div>
</div>



<div class="item">
<img src="images/trucks_5.jpg" alt="">
<div class="carousel-caption">
<h1>On-time Delivery</h1>	
<p>On-time delivery of your goods.</p>
</div>
</div>



</div>

<!-- Controls -->
<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
<span class="glyphicon glyphicon-chevron-left"></span>
</a>
<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
<span class="glyphicon glyphicon-chevron-right"></span>
</a>
</div>
</div>






<div class="container margin_top25" id="home_intro">
<h2 class="blue_text">A Brief Introduction</h2>
<div class="row">

<div class="col-sm-6">
<p><strong>SDB investments Limited</strong> is one of <strong>Uganda's</strong> leading goods/cargo transportation company with over <strong>15 years experience</strong> in the field. We also buy and sell agricultural produce.
Our objective is to provide the best possible transportation services available along with competitive pricing.
</p>

<p>
With our highly-experienced, professional workforce of drivers and administrators, fully-equipped infrastructure and excellent communication network, SDB investments Limited has shaped itself as an organized corporation driven by a full-service policy. 
</p>

<p>
We transport over 200 loads annually across all major East African countries including: <strong>Uganda, Kenya, South Sudan, Tanzania, Rwanda, Burundi and DRC (Democratic Republic of Congo).</strong>
Our customers benefit from our broad transportation network of over 30 trucks. All our trucks are <strong>Mercedez-Benz Actros,</strong> which are the industry standard in the mordern transportation industry due to their unmatched <strong>reliability, speed and safety.</strong>
</p>

<p>
With one of the fastest growing staffs of logistics professionals in the industry, we deliver best-in-class service to each of our customers. SDB investments Limited ensures that every freight move is optimized and tendered at the lowest cost, meeting service requirements while providing increased visibility through<strong> tracking, administration and reporting.</strong> We provide the right solution for your business, adapting to your specific needs and requirements.
</p>

<p>
<strong>Experience. Capacity. Flexibility.</strong> SDB investments Limited provides everything your business needs to gain a competitive edge in today's rapidly changing transportation environment.
</p>
</div><!-- col div -->


<div class="col-sm-6">
<img class="img-responsive" src="images/service_area.png">
</div><!-- col div -->


</div><!-- row div -->
</div>











<div class="container margin_top25" id="home_values">
<h2 class="blue_text"> Our Core Business Values </h2>
<div class="row">

<div class="col-sm-4">
<p><strong>Customer satisfaction</strong></p>
<p>At SDB investments Limited, customer satisfaction ranks highly on our list of priorities. Our customers mean everything to us. We believe that, "To give real service you must add something which cannot be bought or measured with money, and that is sincerity and integrity. " --Don Alden Adams.
</p>
<p><a></a></p>
</div><!-- col div -->


<div class="col-sm-4">
<p><strong>Quick, on-time delivery</strong></p>
<p>On-time delivery is critical in our business and yours. To meet your on-time requirements consistently, we identify the best ground routes. At SDB Investments Limited, we optimize transportation and reduce waste by providing reduced costs and improved labour productivity to your business.</p>
<p><a></a></p>
</div><!-- col div -->

<div class="col-sm-4">
<p><strong>Cargo safety</strong></p>
<p>SDB Investments Limited is very attentive to cargo safety. As accidents are more likely to happen with oversized cargoes, we provide a detailed cargo assessment to ensure cargo safety on board. </p>
<p><a></a></p>
</div><!-- col div -->

</div><!-- row div -->
</div>





<!--
<div class="container">
<h3>Area of service</h3>
<p>
We transport goods to and from any East African country including Uganda, Kenya, Tanzania, Rwanda ,South Sudan and Democratic Republic of Congo (DRC).
Our clients from the above mentioned countries are a testament to the fact. Our cherry-picked, well experienced drivers equiped with our fleet of the highly reliable Mercedez Benz Actros trucks get the job done everytime.
</p>
</div>

<div class="container text-center">
<img class="img-responsive" src="images/service_area.png">
</div>
-->















<?php
include("./subViews/footer.php");
?>