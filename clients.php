<?php 
include("./subViews/header1.php");
?>
<title>Our Clients</title>
<?php 
include("./subViews/header2.php");
?>


<div class="jumbotron white_text">
<div class="container">
<h2>OUR CLIENTS</h2>
</div>
</div>




<div class="full_width">
<div class="container">

<div class="row">
<div class="col-sm-4">
<p><img class="img-responsive" src="images/wfp_small.jpg"></p>
<h2>World Food Programme</h2>
<p>Kampala, Uganda</p>
</div><!-- col div -->

<div class="col-sm-4">
<p class="height_100"><img class="img-responsive" src="images/aponye.png"></p>
<h2>Aponye Uganda Limited</h2>
<p>Kampala, Uganda</p>
</div><!-- col div -->

<div class="col-sm-4">
<p class="height_100"><img class="img-responsive" src="images/pearl.png"></p>
<h2>Pearl Diary Farms Limited</h2>
<p>Kampala, Uganda</p>
</div><!-- col div -->
</div><!-- row div -->




<div class="row">
<div class="col-sm-4">
<div class="imp_logo" id="imp_logo1">
<p class="imp_logo_name" >Kamuru Trading Company</p>
<p>Kigali, Rwanda</p>
</div>
</div><!-- col div -->

<div class="col-sm-4">
<div class="imp_logo" id="imp_logo2">
<p class="imp_logo_name" > Kam Suppliers & Contractors Uganda Ltd</p>
<p>Kampala, Uganda</p>
</div>
</div><!-- col div -->

<div class="col-sm-4">
<div class="imp_logo" id="imp_logo3">
<p class="imp_logo_name" >Kabuye Sugar Works Ltd</p>
<p>Kigali, Rwanda</p>
</div>
</div><!-- col div -->
</div><!-- row div -->




<div class="row">
<div class="col-sm-4">
<div class="imp_logo" id="imp_logo4">
<p class="imp_logo_name" >Lukak Stores</p>
<p>Juba, South Sudan</p>
</div>
</div><!-- col div -->

<div class="col-sm-4">
<div class="imp_logo" id="imp_logo5">
<p class="imp_logo_name" > Dayreel General Trading Co Ltd</p>
<p>Juba, South Sudan</p>
</div>
</div><!-- col div -->

<div class="col-sm-4">
<div class="imp_logo" id="imp_logo6">
<p class="imp_logo_name" >Premier Commodities Uganda Limited</p>
<p>Kampala, Uganda</p>
</div>
</div><!-- col div -->
</div><!-- row div -->












</div><!-- container div -->
</div><!-- full_width div -->




<?php
include("./subViews/footer.php");
?>