<?php 
include("./subViews/header1.php");
?>
<title>Contact Us</title>
<?php 
include("./subViews/header2.php");
?>


<div class="jumbotron white_text">
<div class="container">
<h2>CONTACT US</h2>
</div>
</div>



<div class="full_width padding_bottom">
<div class="container">
<div class="panel panel-default">
<div class="panel-body">



<div class="row">
<div class="col-sm-6">
<h3 class="blue_text">Phone No:</h3>
<h5> +256 701 990 410 </h5>
<h5> +256 702 558 078 </h5>
</div><!-- col div -->

<div class="col-sm-6">
<h3 class="blue_text">Instant Messaging:</h3>
<h5>Whatsapp: +256 701 990 410 <strong>OR</strong> +256 702 558 078 </h5>
<h5>Viber: +256 701 990 410 <strong>OR</strong> +256 702 558 078 </h5>
</div><!-- col div -->
</div><!-- row div -->




<div class="row">
<div class="col-sm-6">
<h3 class="blue_text">Social Media:</h3>
<a href="https://web.facebook.com/sdbinvestmentsugandaltd"><h5 class="blue_text"> Facebook </h5></a>
<h5 class="blue_text"> LinkedIn </h5>
<h5 class="blue_text"> Twitter </h5>
<h5 class="blue_text"> Yelp </h5>
</div><!-- col div -->

<div class="col-sm-6">
<h3 class="blue_text">Email:</h3>
<h5>sbdinvestments@gmail.com </h5>
</div><!-- col div -->
</div><!-- row div -->




<div class="row">
<div class="col-sm-6">


<h3 class="blue_text">Postal Address:</h3>
<h5>P.O. BOX 34682</h5>
<h5>Kampala, Uganda</h5>

<h3 class="blue_text">Physical Address:</h3>
<h5>Plot no. 590</h5>
<h5>Wankulukuku Road</h5>
<h5>Nalukolongo, Kampala</h5>
<h5>Uganda</h5>
<div>

</div>

</div><!-- col div -->

<div class="col-sm-6">
<img src="images/sdb_location.png" class="img-responsive">
</div><!-- col div -->
</div><!-- row div -->





</div>
</div>
</div><!-- container div -->
</div><!-- full_width div -->



<?php
include("./subViews/footer.php");
?>